const mongoose = require('mongoose'),
Schema = mongoose.Schema,
postSchema = new Schema({
	author: {type:String,required:true,trim:true},
	author_id: {type:String,required:true},
	headline: {type:String,required:true,trim:true},
	tags: {type:Array,trim:true},
	views: {type:Number,default:0},
	takes: {type:Number,default:0},
	finished: {type:Number,default:0},
	gained: {type:Number,default:0},
	loss: {type:Number,default:0},
	entry_sug: {type:Number,trim:true},
	finished: Date,
	updated_at: Date,
	created_at: {type:Date,required:true} 
});

postSchema.pre('save',function(next) {
	const post = this;
	next();
})

const Post = mongoose.model('Post',postSchema);

module.exports = Post;