const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tagSchema = new Schema({
	tag: {type:String,required:true},
	numberOfTags: {type:Number,required:true,default:0},
	lastCreated: {type:Date,required:true},
	searched: {type:Number,default:0},
	popular: {type:Number,default:0}
})

tagSchema.pre('save',function(next) {
	const tag = this;
	next();
})

const Tags = mongoose.model('Tags',tagSchema);

module.exports = Tags;