const mongoose = require('mongoose'),
bcrypt = require('bcrypt'),
Schema = mongoose.Schema,
userSchema = new Schema({
	username: {type: String,unique: true,required:true,trim: true},
	password: {type: String,required:true},
	firstname: {type:String,required:true},
	lastname: {type:String,required:true},
	notifications: {type:Array},
	followers: {type:Number,required:true,default:0},
	access_p: {type:Boolean,required:true,default: false},
	suggs: {type:Number,default: 500},
	//half created challanges
	nf_af: {type:Number,required:true,default: 0},
	//full created challanges 
	created_af: {type:Number,required:true,default:0},
	created_at: {type:Date},
	updated_at: {type:Date}
});

userSchema.statics.authenticate = (username,password,callback) => {
	User.findOne({username})
		.exec((err,user) => {
			if (err) {
				return callback(err)
			} else if(!user) {
				var err = new Error('User not found!');
				err.status = 401;
				return callback(err);
			}
			bcrypt.compare(password,user.password,(error,result) => {
				if (result === false) {
					var err = new Error('User not found!');
					err.status = 401;
					return callback(err);
				}
				if (result === true) {
					callback(null,user)
				}
			})
		})
}

userSchema.pre('save',function(next) {
	const user = this;
	bcrypt.hash(user.password,10,(err,hash) => {
		if (err) {
			return next(err)
		}
		user.password = hash;
		user.updated_at = Date.now();
		next();
	})
})

const User = mongoose.model('User',userSchema);

module.exports = User;