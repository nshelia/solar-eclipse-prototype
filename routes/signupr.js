const express = require('express');
const router = express.Router();
const User = require('../models/server')

let errorD = (msg,res,req) => {
	var err = new Error(msg)
    err.status = 401
	res.render('signup-finished',{title: 'Sign up',err,add: "Please enter some information to be succeed",ex: "Back"})
}

router.get('/finished',(req,res,next) => {
	if (req.session.user) {
		return res.redirect('/')
	}	
	if(req.session.active) {
		return res.render('signup-finished',{title: 'Sign up',add: "Please enter some information to be succeed",ex: "Back"})
	}
	if (req.session.userId || req.session.a) {
		return res.redirect('/')
	}
	res.redirect('/signup');
})

router.post('/finished',(req,res,next) => {
	const user = {
		username: req.body.username,
		password: req.body.password,
		firstname: req.session.active.fname,
		lastname: req.session.active.lname,
		created_at: new Date()
	}
	if (user.username && user.password) {
		if (user.username.length > 3) {
			if (user.password.length > 5) {
				if (user.password === req.body.cpassword) {
					User.create(user,(err,user) => {
						if (err) {
							if (err.code === 11000) {
								errorD('Username already exists',res,req)
							}
						} else {
							req.session.userId = user._id;
							req.session.a = user;
							return res.redirect('/');
						}
					})
				} else {
					errorD('Passwords do not match',res,req);
				}
			} else {
				errorD('Your password must be at least 6 characters long',res,req);
			}
		} else {
			errorD('Your username must be at least 4 characters long',res,req);
		}
	} else {
		errorD('All fields required',res,req);
	}
})

module.exports = router;