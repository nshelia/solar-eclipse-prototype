const express = require('express'),
router = express.Router(),
_ = require('underscore'),
User = require('../models/server'),
Posts = require('../models/suggestions');

router.get('/profile/user',(req,res,next) => {
	if (req.session.api_access) {
		return User.findById(req.session.authUser,error,doc => {
			let user = {};
			user._id = doc._id;
			user.username = doc.username;
			user.firstname = doc.firstname;
			user.lastname = doc.lastname;
			user.suggs = doc.suggs;
			user.access_p = doc.access_p;
			user.followers = doc.followers;
			res.end(JSON.stringify(user));
		})
	}
	res.redirect('/');
})
module.exports = router;