const express = require('express');
const router = express.Router();
const Posts = require('../models/suggestions');
const User = require('../models/server')
router.get('/',(req,res,next) => {
	if (req.session.active) {
		delete req.session.active;
	}
	if(req.session.a) {
		delete req.session.a;
	 	return res.render('index',{title:"Login",add: "You have successfuly registered",ex: "Create new account"})
	}
	if (req.session.user) {
		let promise = User.findById(req.session.userId).exec();
		promise.then(function(user) {
			req.session.authUser = user;
			if (user.notifications.length === 0) {
				return res.render('home',{user: req.session.authUser,notEmpty: true});
			}
			res.render('home',{user: req.session.authUser});
		})
	} else {
		res.render('index',{title:"Login",ex: "Create new account"})
	}	
	
})
router.post('/',(req,res,next) => {
	var conditions = { _id: req.session.userId }
  , update = { $inc: {nf_af: 1 }};
  	let user_live_suggs;
  	if (req.body.home === 'a_access') {
  		let live_suggs_promise = User.findById(req.session.userId).exec();
  		live_suggs_promise.then(function(user) {
  			if (user.suggs >= 200 ){
  				return res.end('true');
  			}
  			res.end('false');
  		}).catch(function(error) {
  			if (error) {
  				return next(error);
  			}
  		})
  	} 
	if (req.body.home === 'post') {
		const inData = req.body.post;
		const post = {
		 headline: inData.headline,
		 author: req.session.user.firstname,
		 author_id: req.session.userId,
		 created_at: Date.now()
		}
		Posts.create(post,function(error,post) {
			if (error) {
				return next(error);
			}
			User.update(conditions,update,function(error,user) {
				req.session.postId = post._id;
				res.end();
			})
		})
	} else if (req.body.home === 'post1') {
		const inData = req.body.post1;
		const post1 = {
		 tags: inData.desc,
		 suggs: inData.suggs,
		}
		var conditions = { _id: req.session.postId }
  		, update = {tags: post1.tags,entry_sug: post1.suggs,finished: Date.now()};
		let promise1 = Posts.update(conditions,update).exec();
		promise1.then(function(post) {
			return User.update({_id:req.session.userId},{$inc: {nf_af: -1,created_af: 1,suggs: -200}}).exec();
		}).then(function(user) {
			let live_suggs_promise = User.findById(req.session.userId).exec();
			live_suggs_promise.then(function(data) {
		  		user_live_suggs = data.suggs;
		  		 if (user_live_suggs >= 200) {
				User.update({_id: req.session.userId},{access_p: true},function(error,user) {
					if (error) {
						return next(error);
					}
				});
				} else {
					User.update({_id: req.session.userId},{access_p: false},function(error,user) {
						if (error) {
							return next(error);
						}
					});
				}
		  	})
			res.end();
		})
	} else if (req.body.home === 'back') {
		Posts.find({_id: req.session.postId}).remove(function(err) {
		})
		User.update({_id:req.session.userId},{$inc: {nf_af: -1}},function(error,user) {
			delete req.session.postId;
			res.end();
		})
		
	} else if (req.body.password || req.body.username) {
		delete req.session.userId;
		delete req.session.a;
		delete req.session.active;
		const password = req.body.password;
		const username = req.body.username;
		if (password && username) {
			User.authenticate(username,password,function (error,user) {
				if (user) {
					req.session.user = user;
					req.session.userId = user._id;
					req.session.api_access = true;
					if (req.session.user.suggs >= 200) {
						User.update({_id: req.session.userId},{access_p: true},function(error,user) {
							if (error) {
								return next(error);
							}
							res.redirect('/');
						});
					} else {
						User.update({_id: req.session.userId},{access_p: false},function(error,user) {
							if (error) {
								return next(error);
							}
							res.redirect('/');
						});
					}
				} 
				if (error) {
					var err = new Error("User not found")
					err.status = 401;
					res.render('index',{title:"Login",err,ex:"Create new account"})
				}
			})
		} else {
			var err = new Error("All fields required")
			err.status = 401;
			res.render('index',{title:"Login",err,ex:"Create new account"})
		}
	}
})
router.get('/logout',(req,res,next) => {
	if (req.session.user) {
		req.session.destroy();
		res.redirect('/');
	}
})
router.get('/signup',(req,res,next) => {
	if (req.session.user) {
		return res.render('home',{user: req.session.user});
	}		
	if (req.session) {
		req.session.destroy();
	}
	res.render('signup',{title: "Sign up",ex: "Log in with existing account"})
})
router.post('/signup',(req,res,next) => {
	const fname = req.body.firstname;
	const lname = req.body.lastname;
	req.session.active = {fname,lname};
	if (fname && lname) {
		res.redirect('/signup/finished')
	} else {
		var err = new Error("All fields required")
		err.status = 401;
		res.render('signup',{title:"Sign up",err,ex:"Log in with existing account"})
	}
})
module.exports = router;