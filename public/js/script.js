//initial js-styles
const w = $('#workshop'),
n = $('#net'),
t_icon = $('.tag-icon'),
headline = `<div class="column-12 reliv"><span class="headline-icon pos-a"></span><input class="mod b-r" type="text" name="headline" placeholder="Name your challange" id="headline"/></div>`,
starter_net = `<div class="row cr">
	    				<div class="column-12">
	    					<button id="starter" class="hbo c-p fn-b">Create</button>
	    				</div>
	   				 </div>
	  			</div>`,
w_net = `<h4 class="il c-p stage-title">Workshop</h4>
		  <div class="column-12" id="net">`,
w_s_t = $('#workshop .stage-title'),
s_c = $('.suggs-count'),
slb = (el) => el.css('display','block'),
sln = (el) => el.css('display','none');

if (s_c.html() === '0') {
	s_c.css('color','rgba(226,34,34,1)');
}
//end
$('body').css('display','none');
$('body').fadeIn(350);

$('body').on('click','#starter',e =>  {
	const headlineValue = $('#headline').val();
	let post = {
		headline: headlineValue
	}
	if (post.headline) {
		$.ajax({
			url: '/',
			method: 'POST',
			data: {home: 'post',post},
	        complete: () => {	
        		$('#net').fadeOut(300,() => {
	        		$('#net').remove();
	        		$('#workshop .stage-title').remove();
	        		w.append(`<h4 class="il c-p stage-title">Costumize</h4>
	        		<span class="back-first fn-b c-p inl-b" id="back-first-c">Back</span>
	        		<div class="column-12" style="position:relative;" id="net">`);
	        		const describe = `<div class="column-12 reliv">	
					        				<span class="tag-icon pos-a"></span>
					        				<input type="text" value="" name="abouter" placeholder="Enter some tag" id="tags">
					        			<div class="tag-input">
					        			</div>
					        		</div>`;
	        		$('#net').append(describe);
	        		const entrysuggs = `<div class="column-12" id="ensuggs-cont"><span class="suggs-icon pos-a" id="suggs-icon-c"></span><input class="mod fn-b t-c disb" type="number" name="suggs" id="ensuggs" placeholder="Entry suggs" maxlength="3" min="1" max="999"></div>`;
	        		$('#net').append(entrysuggs);
					$('#net').append(`<button class="hbo c-p fn-b" id="qi">Finish</button>`);
					let dataArray = [];
					$('#tags').keyup(e => {
						let $this = $(e.currentTarget);
						if (e.keyCode === 13 && dataArray.length < 5) {
							$('.tag-input').append(`<span class="tagc">#${$this.val()}</span>`);
							dataArray.push($this.val());
							$this.val("");
							if (dataArray.length === 4) {
								sln($this);
								sln(t_icon);
							} else {
								$this.fadeIn(300);
								slb(t_icon);
							}
							$('.tagc').click(e => {
								let $this = $(e.currentTarget),
								index = $this.index();
								$this.fadeOut(300,() => {
									$this.remove();
									dataArray.splice(index,1);
									slb($('#tags'));
									slb(t_icon);
								})
							})	
						}					
					})
					$('#ensuggs').keypress(e => {
						let $this = $(e.currentTarget);
						if (Number($this.val()) > 99) {
							$this.val("")
						} else if(Number($this.val()) < 0) {
							$this.val("");
						}
					})
					$('.back-first').click(() => {
						$.ajax({
							url: '/',
							method: 'POST',
							data: {home: 'back'},
							complete: function() {
								$('#net').fadeOut(300,() => {
									$('#net').remove();
									$('#workshop .stage-title').remove();
									$('.back-first').fadeOut(300);
									w.append(w_net);
									$('#net').append(headline);
									$('#net').append(starter_net);								   
								})
							}
						})
					})	
					$('#qi').click(e => {
						const suggs = Number($('#ensuggs').val());
						let post1 = {
							desc: dataArray,
							suggs
						}
						if (post1.suggs && post1.desc.length !== 0) {
							if (post1.desc.length < 120) {
								$.ajax({
									url: '/',
									method: 'POST',
									data: {home: 'post1',post1},
							        complete: () => {	
						        		$('#net').fadeOut(300,() => {
						        			$('#net').remove();
											$('#workshop .stage-title').remove();
											$('.back-first').fadeOut(300);
											w.append(`<h4 class="il c-p stage-title" style="color: #2cc17b">Post has been created</h4>
											<div class="column-12" id="net">`);
											$('#net').append(headline);
											$('#net').append(starter_net);
											setTimeout(() => {
												$('#workshop .stage-title').fadeOut(300,() => {
													$('#workshop .stage-title').html('Workshop').css('color','white');
													$('#workshop .stage-title').fadeIn(300);
												})
											},1500)
											s_c.fadeOut(300,() => {
													let value = s_c.html();
													s_c.html(value - '200')
													s_c.fadeIn(300); 			
											})	  											        		
						        		})
							        }
								})	
							}
						}
					})			
        		})
	        }
		})	
	}
})
$("#tags").keypress((e) => {
	$('.tag-input').append(`<span class="tagcr f-r">#</span>`)
	$('.tag-input tagcr').html('#' + $(e.currentTarget).val());
})
let id = 0;
let timeout2 = null;
$('#afc').click((e) => {
	clearTimeout(timeout2);
	if (id === 0) {
		$.post('/',{home: 'a_access'})
		.done(res => {
			if (res === 'true') {
				w.fadeOut(50);
				$('#afc-container').fadeOut(50,() => {
					$(e.currentTarget).remove();
					w.append(w_net);
					$('#net').append(headline);
					$('#net').append(starter_net);
					w.fadeIn(300);  	
				})
			} else {
				$('#afc').css('border','1px solid rgba(226,34,34,1)')
				$('#afc span').html(`You need at least 200 suggs`);
				id = 1;
			}
		})
	} else {
		clearTimeout(timeout2);
		timeout2 = setTimeout(() => {
			$('#afc').css('border','1px solid #2cc17b')
			$('#afc span').html(`Create an artificial friend`);
			id = 0;
		},2000)
	}
})
