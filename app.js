// Topics 
//   Book - D
//     Timer - 24
//       Quiz - 4
//         Points - D
//          Get Coins - D
//   Movie - D
//     Timer - 24
//       Quiz - 4
//         Points - D
//          Get Coins - D
//   Training - D
//     Timer - 24
//       Quiz - 4
//         Points - D
//          Get Coins - D
// Get Coins - D
//   Education - D
//     Timer - 24
//       Quiz - 4
//         Points - D
//          Get Coins - D
// When you are taking challange you are going to talk to AI it will get

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const session = require('express-session')
mongoose.connect('mongodb://nshelia:qardsqards@ds127783.mlab.com:27783/nswer',err => {
	if (err) throw err;
})
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
}))
app.use((req,res,next) => {
  res.locals.lent = req.session.a;
  res.locals.uID = req.session.userId;
  next();
})
const index = require('./routes/index')
const signupr = require('./routes/signupr')
const api = require('./routes/api');
app.set('views',__dirname + '/views');
app.set('view engine','pug');
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.use('/',index);
app.use('/signup',signupr)
app.use('/api',api)

app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3000);